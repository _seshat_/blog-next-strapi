
#### Workshop Strapi HEADLESS CMS Back + Frameworks Js Front (React / Next , Vue / Nuxt / Gridsomes)

--- 

##### Intro

Créer 3 dossiers sur vos machines

- blog
- ecommerce
- corporate

##### 3 Templates

A l'intèrieur de chacun, mettez en place chacun des templates suivant

pour le blog vous pouvez choisir entre 5 technos disponible


###### 1 : Blog

Front
https://github.com/strapi/strapi-starter-next-blog
https://github.com/strapi/strapi-starter-gridsome-blog
https://github.com/strapi/strapi-starter-react-blog
https://github.com/strapi/strapi-starter-vue-blog

Back
https://github.com/strapi/strapi-template-blog


###### 2 : Ecommerce

Front
https://github.com/strapi/strapi-starter-nuxt-e-commerce
https://github.com/strapi/strapi-starter-next-ecommerce

Back
https://github.com/strapi/strapi-template-ecommerce


###### 3 : Corporate

Front
https://github.com/strapi/strapi-starter-next-corporate

Back
https://github.com/strapi/strapi-template-corporate

**Cadeau**: https://gitlab.com/_seshat_/next-strapi
- docker-compose up --build
---

###### Step 1

- install nvm 

`curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash`

- add nvm script to .bashrc

`export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm`

- install node version 12

`nvm install 12`

- use node version 12 

`nvm use 12`

- install yarn

`npm install -g yarn`

- install strapi **nuxt** blog starter (vuejs)

`yarn create strapi-starter my-site nuxt-blog`

- install strapi **next** blog starter (react)

`yarn create strapi-starter my-site next-blog`

---

##### Step 2 - Normal

###### Comments
- creer une collection commentaire depuis le back office strapi
- ajouter les champs
    - texte
    - date
    - relation - article has many commentaires
- ajouter un component commentaire a la page article /pages/article/[slug].js
- docs css UIKit : https://getuikit.com/docs/introduction
- docs nextjs : https://nextjs.org/docs | https://www.freecodecamp.org/news/the-next-js-handbook/
- docs nuxtjs : https://nuxtjs.org/docs/concepts/views
- afficher les commentaires en dessous de l'auteur et style votre composent avec la docs uikit et du simple css
###### Map
- creer une collection map
- ajouter les champs
    - lat
    - lon
    - relation - article has one map
- installer et integrer le component : https://react-leaflet.js.org/docs/example-map-placeholder/
    - afin d'ajouter une carte qui ciblera le point correspondant a votre latitude et longitude 
###### Theme
- implementer des animations sur les images et les titres des articles et des categories dispo sur le site grace a : https://getuikit.com/docs/animation
- remplacer le css par du sass via : https://medium.com/officialrajdeepsingh/how-to-add-sass-scss-in-next-js-77a2b34f1ff3

---

###### Step 2 - Advanced

- a partir de ces ressources implementer un chat avec les websocket : 
    - **tutorial** : https://strapi.io/blog/how-to-build-a-real-time-chat-forum-using-strapi-socket-io-react-and-mongo-db
    - dans un dossier react en avec les autres dossiers 
            - frontend
            - backend
            - *creer un dossier frontend-react*
                - creer une *nouvelle app react* et non next (qui est un framework SSR)
```sh
npx create-react-app my-app
cd my-app
npm start
```
    - en suivant ce tutoriel
        - ajouter la fonctionnalite de websockets a votre backend strapi 
        - implementer le client chat dans cette nouvelle app (plutot)

- attention on n'utilise pas mongodb mais strapi avec sqlite du coup il faudra faire les modifications equivalentes

- version simple : https://www.scriptverse.academy/tutorials/reactjs-chat-websocket.html 

---
